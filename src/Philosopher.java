// Philosopher
import java.util.Random;
/** the Dining Philosophers.
  */
public class Philosopher extends Thread {
  /** the fork between two philosophers.
    */
  protected static class Fork {
    protected int me;		// number for trace
    protected boolean inUse;	// true if fork is in use
    public Fork (int me) {
      this.me = me;
    }
    /** returns true if fork is obtained, false if not.
      */
    public synchronized boolean get (int who) {
      System.err.println(who+(inUse ? " misses " : " grabs ")+me);
      return inUse ? false : (inUse = true);
    }
    /** drops the fork.
      */
    public synchronized void put (int who) {
      System.err.println(who+" drops "+me);
      inUse = false; notify();
    }
    /** returns once fork is obtained.
      */
    public synchronized void waitFor (int who) {
      while (! get(who))
	try {
	  wait();
        } catch (InterruptedException e) { e.printStackTrace(); }
    }
  }
  /** make one diner.
    */
  public Philosopher (int me, Fork left, Fork right) {
    this.me = me; this.left = left; this.right = right;
  }
  protected static Random random = new Random();	// randomize
  protected int me;		// number for trace
  protected Fork left, right;	// my forks
  /** philosopher's body: think and eat 5 times.
    */
  public void run () {
    for (int n = 1; n <= 5; ++ n) {
      System.out.println(me+" thinks");
      try {
	Thread.sleep((long)(random.nextFloat()*1000));
      } catch (InterruptedException e) { e.printStackTrace(); }
      for (;;)
	try {
	  left.waitFor(me);
	  if (right.get(me)) {
	    System.out.println(me+" eats");
	    try {
	      Thread.sleep((long)(random.nextFloat()*1000));
	    } catch (InterruptedException e) { e.printStackTrace(); }
	    right.put(me);
	    break;
	  }
	} finally {
	  left.put(me);
	  Thread.yield();	// necessary to reschedule sparc ultra...
	}
    }
    System.out.println(me+" leaves");
  }
  /** sets up for 5 philosophers.
    */
  public static void main (String args []) {
    Fork f[] = new Fork[5];
    for (int n = 0; n < 5; ++ n) f[n] = new Fork(n);
    Philosopher p[] = new Philosopher[5];
    p[0] = new Philosopher(0, f[4], f[0]);	// backwards
    for (int n = 1; n < 5; ++ n) p[n] = new Philosopher(n, f[n-1], f[n]);
    //f[3].inUse=true;
    for (int n = 0; n < 5; ++ n) p[n].start();
  }
}